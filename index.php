<?php

require_once '_functions.php';

$search_words = array('php','html','интернет','web');

$search_strings = array(
'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',
'PHP - это распространенный язык программирования  с открытым исходным кодом.',
'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML.'
);


$arr = searchWords($search_words, $search_strings);

if(is_array($arr)){
    foreach ($arr as $item){
        echo $item.'<br>';
    }
}else{
    echo $arr;
}