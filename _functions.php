<?php

function searchWords($words, $text){

    foreach ($text as $key => $sentence) {

        $str = 'В предложении №' .($key+1). ' есть слова: ';
        $found = false;

        foreach ($words as $word) {

            if (preg_match('/\b' . $word . '\b/iu', $sentence)) {
                $str .= $word . ', ';
                $found = true;
            }
        }

        if ($found) {
            $result[] = $str;
        }

    }

    if(!isset($result)){
        $result ='В тексте нет таких слов';
    }

    return $result;
}